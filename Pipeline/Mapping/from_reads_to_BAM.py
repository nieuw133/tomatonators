#!usr/bin/env python3
"""
========================================================================
Author: Tomatonators
Script to parse parameters file, parse Hisat2 variables, run Hisat2,
and run samtools.
========================================================================
How to use this program:
cmd = python3 <program name> <txt file with parameters>

Firstly, variables are obtained from the parameters tekst file with
parameter_parser. Variables from two categories are parsed into a 
dictionary: "samples" and "hisat2". 
    - "samples" -- locations of all reads
    - "hisat2" -- genome version, organism name and locations of indexes

Secondly, the specified format for the reads and index is parsed from 
the dictionaries. Reads are filtered for the correct organism and the
index location for genome version.

Thirdly, hisat2 runs for each experiment, outputting a .sam file for
every experiment.

Finally, samtools sort creates a .bam file for each .sam file that
has been created
========================================================================
"""
# imports

import sys
import subprocess
import os.path

# formula
def parameter_parser(filename, function_name):
    """ Returns dictionary of {variable_name: variable} from a function
    
    Keyword arguments:
        filename -- string, name of a file
    Returns:
        dictionary -- dictionary, key = variable name: value = variable
    """
    boleean = False
    dic = {}
   
    with open(filename) as f:
        for line in f:
            line = line.strip()
            if line.startswith(function_name+":"):
                boleean = True
            elif not line:
                boleean = False
            elif boleean == True:
                temp_list = line.split(" = ")
                dic[temp_list[0]] = temp_list[1]
    #print (dic)
    return dic


def parser_hisat2(hisat2_vars, sample_vars):
    """ Parses the forward and reverse reads for hisat2 input
    
    Keyword arguments:
        sample_vars -- dictionary, dictionary with all sample variables
        hisat2_vars -- dictionary, dictionary with all hisat variables
    Returns:
        forward -- dictionary, dictionary with of forward file locations
        reverse -- dictionary, dictionary with of reverse file locations
    """
    forward = {}
    reverse = {}
    index = hisat2_vars["g_location"]+hisat2_vars["genome"]

    for i in range(len(sample_vars)):
        dic_key = list(sample_vars)[i]
<<<<<<< HEAD:Pipeline/hiset2_program.py
        if dic_key.startswith(organism):
            if dic_key.endswith("f"):
                forward[dic_key] = sample_vars[dic_key]
            elif dic_key.endswith("r"):
                reverse[dic_key] = sample_vars[dic_key]
    counter = 0
    for i in range(len(hisat2_vars)):
        dic_key = list(hisat2_vars)[i]
        counter +=1
        if dic_key.startswith(genome):
            index += hisat2_vars[dic_key][0:77]
            break
    print (counter)
    print (forward)
=======
        if dic_key.endswith("f"):
            forward[dic_key] = sample_vars[dic_key]
        elif dic_key.endswith("r"):
            reverse[dic_key] = sample_vars[dic_key]

>>>>>>> c24f0fa0652e62fc61e1bf5d35bdd55c54abe105:Pipeline/Mapping/from_reads_to_BAM.py
    return index, forward, reverse


def run_hisat2(indx, f_reads, r_reads):
    """ Runs Hisat2
    
    Keyword arguments:
        indx -- string, file location of a single index file
        f_reads -- dictionary, dictionary with of forward file locations
        r_reads -- dictionary, dictionary with of reverse file locations
    Returns:
        outs -- list of SAM files
    """
    outs=[]
    for i in range(len(f_reads)):
        f_read = list(f_reads.values())[i]
        r_read = list(r_reads.values())[i]
        out = list(f_reads.keys())[i][0:7]+".sam"
        outs.append(out)

        cmd = 'hisat2 -x {} --dta -1 {} -2 {} -S {}'.format(\
            indx, f_read, r_read, out)

        if os.path.exists(out):
            print(out+"\n^^^ Hisat2 file already exists ^^^")
            continue

        subprocess.check_call(cmd, shell = True)
        print(out+" has been created! :)")
                
    return outs

def turn_bam(in_fn):
    """
    run samtool sort

    Key argurments:
        in_fn: list with .sam files
    returns:
        filenames
    """
    for i in in_fn:
        if os.path.exists('{}_sorted.bam'.format(i)):
            print("already sorted and bamed")
        else:
            cmd = 'samtools sort {} > {}_sorted.bam'.format(i,i)
            e = subprocess.check_output(cmd, shell=True)
            print("{} sort and bam!".format(i))
    print("BAM-files created")


# main

if __name__ == "__main__":

    filename = sys.argv[1]

    # step 1 and 2: 

    index_files, f_reads, r_reads = parser_hisat2(\
                    parameter_parser(filename, "hisat2"),\
                    parameter_parser(filename, "samples"))

    # step 3

<<<<<<< HEAD:Pipeline/hiset2_program.py
    #run_hisat2(index_files, f_reads, r_reads)
=======
    outs = run_hisat2(index_files, f_reads, r_reads)

    # step 4

    turn_bam(outs)
>>>>>>> c24f0fa0652e62fc61e1bf5d35bdd55c54abe105:Pipeline/Mapping/from_reads_to_BAM.py



