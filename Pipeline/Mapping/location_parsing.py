#!/usr/bin/env python3
"""
author: Xiaofei Yu
scripts that used for parsing the config.txt
"""

#imports 
from sys import argv 
import os.path 

#functions 
def parsing_inputfile(data_location):
	""" parse the input data location
	
	input: data_location, txt
	output1: forward_strains, list with 1.fastq.gz
	output2: reverse_strains, list with 2.fastq.gz
	"""
	forward_strains = []
	reverse_strains = []
	counter = 0
	with open(data_location, "r") as file_to_read:
		for line in file_to_read:
			#print(line)
			line = line.strip()
			if not line:
				continue
		
			elif line.startswith("samples"):
				counter +=1
				
			elif counter == 1:
				sample_id = line.split("=")[0]
				forward_strain = line.split("=")[1]
				counter +=1
				
			elif counter == 2: 
				reverse_strain = line.split("=")[1]
				counter = 0
			else: 
				forward_strains.append(forward_strain)
				reverse_strains.append(reverse_strain)
	print (forward_strains,reverse_strains)
	return forward_strains,reverse_strains
		


def main():
	#step1: parse the input data location
	forward_strains,reverse_strains = parsing_inputfile(argv[1])
				
				
#main

if __name__ == "__main__":

    main()	
				
				
				
