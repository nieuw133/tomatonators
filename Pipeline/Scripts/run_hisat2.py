"""

"""
import subprocess as sp
import os
import sys


def run_hisat2(indx, f_reads, r_reads):
    """ Runs Hisat2

    Keyword arguments:
        indx -- string, file location of a single index file
        f_reads -- dictionary, dictionary with of forward file locations
        r_reads -- dictionary, dictionary with of reverse file locations
    Returns:
        outs -- list of SAM files
    """
    outs=[]
    for i in range(len(f_reads)):
        f_read = list(f_reads.values())[i]
        r_read = list(r_reads.values())[i]
        out = list(f_reads.keys())[i][0:-2]+".sam"
        outs.append(out)

        if os.path.exists(out):
            print(out+"\n^^^ Hisat2 file already exists ^^^")
            continue

        cmd = '/local/prog/hisat2/hisat2 -x {} --dta -p 4 -1 {} -2 {} -S {}'.format(\
            indx, f_read, r_read, out)

        try:
            sp.check_call(cmd.split())
            print(out+" has been created! :)")
        except sp.CalledProcessError as CPE:
            print(f"Your command: {cmd} failed with returncode "\
                f"{CPE.returncode}")
            sys.exit(104)

    return outs
