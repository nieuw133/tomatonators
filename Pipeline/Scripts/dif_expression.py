"""
Author: Ronald Nieuwenhuis

"""
import subprocess as sp
import sys

def run_deseq2_r(csv_file, treshold, pval, samples):
    """ Run desq2 r-script externally

    csv_file: str, filename (absolute path) to csv counts file
    """
    cmd = f"Rscript Deseq2.R {csv_file} {treshold} {pval} {samples}"
    print(cmd)
    try:
        sp.check_call(cmd.split())
    except sp.CalledProcessError as CPE:
        print(f"Your command: {cmd} failed with returncode {CPE.returncode}")
        sys.exit(105)

if __name__ == "__main__":
    print("Nothing to run")
