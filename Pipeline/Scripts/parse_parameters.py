"""
"""

def parser_hisat2(hisat2_vars, sample_vars):
    """ Parses the forward and reverse reads for hisat2 input

    Keyword arguments:
        sample_vars -- dictionary, dictionary with all sample variables
        hisat2_vars -- dictionary, dictionary with all hisat variables
    Returns:
        forward -- dictionary, dictionary with of forward file locations
        reverse -- dictionary, dictionary with of reverse file locations
    """
    forward = {}
    reverse = {}
    index = hisat2_vars["g_location"]+hisat2_vars["genome"]

    for i in range(len(sample_vars)):
        dic_key = list(sample_vars)[i]
        if dic_key.endswith("f"):
            forward[dic_key] = sample_vars[dic_key]
        elif dic_key.endswith("r"):
            reverse[dic_key] = sample_vars[dic_key]

    return index, forward, reverse


def parameter_parser(filename, function_name):
    """ Returns dictionary of {variable_name: variable} from a function

    Keyword arguments:
        filename -- string, name of a file
    Returns:
        dictionary -- dictionary, key = variable name: value = variable
    """
    boleean = False
    dic = {}

    with open(filename) as f:
        for line in f:
            line = line.strip()
            if line.startswith(function_name+":"):
                boleean = True
            elif not line:
                boleean = False
            elif boleean == True:
                temp_list = line.split(" = ")
                dic[temp_list[0]] = temp_list[1]
    #print (dic)
    return dic
