#!/usr/bin/env python3
"""
Author: tomatonators
RNA-seq pipeline
"""
# imports

import sys, os
#from pipelineparser import PipelineParser
from run_hisat2 import run_hisat2
from parse_parameters import parser_hisat2, parameter_parser
from convert_sam_to_bam import turn_bam
from read_counts import run_stringtie, run_prepDE, sample_file
from dif_expression import run_deseq2_r
from DE_parser import DEparser, colum_select

# formula

def pipeline(filename):
    """ RNAseq pipeline with a Workflow object describing the parameters

    Keyword arguments:
        filename -- 
    Returns:

    """
    # Obtaining parameters from config file for Hisat2 and DESeq2
    index_files, f_reads, r_reads = parser_hisat2(
                    parameter_parser(filename, "hisat2"),
                    parameter_parser(filename, "samples"))

    deseq_vars = parameter_parser(filename, "deseq2")

    # mapping reads to reference genome
    outs = run_hisat2(index_files, f_reads, r_reads)
         # returns list of output file names, example: "name.sam"

    # sorts the .sam file and turn it into a .bam file
    bams = turn_bam(outs)
         # returns list of output file names, example: "name_sorted.bam"

    # stringtie for annotating mapped reads to genes
    annotation_file = "/local/data/BIF30806_2018_2/project/"\
                      "falcarindiol_data/genome/ITAG4.0/"\
                      "ITAG4.0_gene_models.gff"

    sample_description = {} # stores .gtf file names
    for bam in bams:
        outfile = bam.split(".sam_sorted.bam")[0] + ".gtf"
        run_stringtie(bam, annotation_file, outfile, 4)
        sample_description[bam.split(".sam_sorted.bam")[0]] = outfile

    sample_log = sample_file(sample_description, "sample_log.txt")
    
    # prepDE getting the counts for the genes
    prepde_path = "/local/data/BIF30806_2018_2/project/groups/"\
                  "tomatonators/stringtie/prepDE.py"

    sample_counts = run_prepDE(prepde_path, sample_log,\
                               "sample_counts.csv")
    
    # DESeq2 for differentially expressed genes
    df, column_names, genes = DEparser(sample_counts)
    df_selected = colum_select(df, column_names, genes, \
                               deseq_vars["run1"].split(','))

    selected_read_counts = \
    f"{'_'.join(deseq_vars['run1'].split(','))}_readcounts.csv"

    df_selected.to_csv(selected_read_counts)

    run_deseq2_r(selected_read_counts, 10, 0.05, deseq_vars["run2"])




def main():
    # Config file input is mandatory
    config_file = sys.argv[1]

    # workflow_parameters = PipelineParser(config_file)
    pipeline(config_file)


# main

if __name__ == "__main__":
    main()
