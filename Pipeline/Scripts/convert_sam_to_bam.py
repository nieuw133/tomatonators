"""
"""
import subprocess
import os

def turn_bam(in_fn):
    """
    run samtool sort

    Key argurments:
        in_fn: list with .sam files
    returns:
        filenames
    """
    outs = []
    for i in in_fn:
        if os.path.exists('{}_sorted.bam'.format(i)):
            print("already sorted and bamed")
        else:
            cmd = 'samtools sort -@ 4 {} > {}_sorted.bam'.format(i,i)
            e = subprocess.check_output(cmd, shell=True)
            print("{} sort and bam!".format(i))
        outs.append('{}_sorted.bam'.format(i))
    return outs
    #print("BAM-files created")
