#!/usr/bin/env python3
"""
Author: Mark Mekken - 20191211
========================================================================
eggNOG produced a file from queary names and protein sequences. These
results are used here to parse a kegg_pathway and kegg_ko(s) 
(orthologous gene(s)) to be searched in an internet browser as:
www.kegg.jp/kegg-bin/show_pathway?<kegg_pathway>+[<keg_ko>]
example: www.kegg.jp/kegg-bin/show_pathway?map00020+ko:K01899+ko:K01648

This script takes a GO_ID cods and enriched genes from a csv file of
differentially expressed genes and creates the previously described URL.

cmd: python3 "kegg parser.py" <all_annotations> <enriched> <GO_ID>
out: kegg_parser_output.txt

WARNING: will overwrite "kegg_parser_output.txt" if it already exists.
========================================================================
"""
# imports
import sys

# formula

def parser_genes(filename, go_id):
    """ Takes the pathway name and enriched genes from excel file
    
    Key arguments:
        -- filename, string: name of excel file
    Return:
        -- dic_genes, dictionary: GO_ID as key, list of genes as value
        -- path, string: name of pathway
    """
    dic_genes = {}
    boolean = False

    with open(filename) as f:
        for line in f:            
            line_as_list = line.strip().split(",")
            
            pathway_check = False
            for element in line_as_list:
                if element == go_id:
                    pathway_check = True


            if pathway_check == True:
                splitted_line = line.split("\"")
                styled_genes = str(splitted_line[3]).replace("'", "")\
                                                    .replace("]", "")\
                                                    .replace("[", "")
                dic_genes[go_id] = styled_genes
                error_control = True
    print(dic_genes)
    return dic_genes

def parser_eggnog(filename, dic_genes, go_id):
    """ Matches genes in filename and parses the kegg_ko
    
    """
    genes_list = dic_genes[go_id]

    genes_ko = {}
    with open(filename) as f:
        for line in f:
            splitted_line = line.split("\t")
            if splitted_line[0][:-2] in genes_list:
                genes_ko[splitted_line[0][:-2]] = splitted_line[8]
    
    string_ko_ids = ""
    for value in genes_ko.values():
        ko_ids = value.split(",")
        for ko_id in ko_ids:
            string_ko_ids += "+"+ko_id
    
    return string_ko_ids


# main

if __name__ == '__main__':

    # fetch given user input and set ouput name
    
    try:
        annotations_file = sys.argv[1] # all eggNOG gene annotations
        all_enriched_file = sys.argv[2] # all enriched genes in pathway
        go_id = sys.argv[3] # enriched proteins
    except IndexError:
        print("="*80+"\nWhoopsy... an index error occurred...")
        a = "The program \"kegg_ani.py\" needs 3 arguments"
        b = "1. File with all eggNOG annotations"
        c = "2. File with enriched genes"
        d = "3. Pathway of interest (example: map03050)"
        print("="*80+"\n"+a+"\n"+b+"\n"+c+"\n"+d+"\n"+"="*80)
        exit()

    # obtain all differentially expressed genes from a pathway
    
    genes_in_pathway = parser_genes(all_enriched_file, go_id)

    # obtain kegg_ko's for each gene in genes_in_pathway
    
    kegg_ko = parser_eggnog(annotations_file, genes_in_pathway, go_id)

    # obtain URL for all ko in pathway
    
    url = "www.kegg.jp/kegg-bin/show_pathway?"
    print("\n{0}{1}{2}\n".format(url, go_id, kegg_ko))





