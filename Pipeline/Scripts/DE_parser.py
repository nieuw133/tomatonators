#!usr/bin/env python3
"""
========================================================================
Author: Tomatonators
Script to parse read_counts file, parse by columns
========================================================================
How to use this program:
cmd = python3 <program name> <txt file> <paramters>

Firstly, variables are obtained from the parameters text file with
parameter_parser. Variables from two categories are parsed into a 
dictionary: "samples" and "hisat2". 
    - "samples" -- locations of all reads
    - "hisat2" -- genome version, organism name and locations of indexes

Secondly, the specified format for the reads and index is parsed from 
the dictionaries. Reads are filtered for the correct organism and the
index location for genome version.

Thirdly, hisat2 runs for each experiment, outputting a .sam file for
every experiment.

Finally, samtools sort creates a .bam file for each .sam file that
has been created
========================================================================
"""

import pandas as pd
import numpy as np
import sys

def DEparser(in_fn):
    df = pd.read_csv(in_fn)
    column_names=list(df)[1:]
    genes=list(df['gene_id'])
    return df, column_names, genes

def colum_select(df, column_names, genes, columns_to_select):
    #l=[]
    #
    #for column_name in column_names:
    #    for selected_column_name in set(columns_to_select):
    #        if column_name == selected_column_name:
    #            l.append()

    selection_df = df.loc[:, columns_to_select]
    selection_df.index = genes
    selection_df.index.name = 'gene_id'
    return selection_df

# main

if __name__ == "__main__":

    #in_fn = sys.argv[1]

    # step 1 and 2: 

    df,whole_l,features = DEparser(in_fn)
    # step 3
    keywords = sys.argv[2].split(',')
    df_selected=colum_select(df,whole_l,features,keywords)

    # step 4
    df_selected.to_csv(r'{}_readcounts.csv'.format('_'.join(map(str,keywords))))
