#!/usr/bin/env python3

"""
Author: Jaccoline Zegers
"""

from sys import argv

def parser(name_file):
	names = []
	f = open(name_file, "r")
	for line in f:
		names.append(line.strip())
	f.close()
	return names
	
def selector(names, fasta_file):
	proteins = []
	for n in names:
		boolean = False
		with open(fasta_file, "r") as f:
			for line in f:
				if boolean:
					boolean = False
					protein = line.strip().replace("*", "")
					proteins.append((name, protein))
				elif line.startswith(">" + n):
					boolean = True
					name = line.strip()			
	return proteins
	
def output(protein_list):
	f = open("pp_out.fasta", "w")
	for x in proteins:
		f.write(x[0] + "\n" + x[1] + "\n")
	f.close()

if __name__ == "__main__":

    # Step 1. Parse names
    names = parser(argv[1])
    
    # Step 2. Select proteins
    proteins = selector(names, argv[2])
    
    # Step 3. Create output
    output(proteins)
    
