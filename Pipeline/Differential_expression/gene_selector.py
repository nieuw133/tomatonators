#!/usr/bin/env python3

"""
Author: Jaccoline Zegers
Script to filter genes on  max padj-value and min log2-fold change.
Input: config file with locations from .tsv files and settings as:
        file: example1.tsv
        file: exmaple2.tsv
        ...
        p_value: number
        fold_change: number
output: csv value with genes, log2fold-changes, and p-values.
"""

from sys import argv

def config_parser(config_file):
	file_list = []
	config = open(config_file, "r")
	for line in config:
		l = line.split()
		if l[0] == "file:":
			file_list.append(l[1])
		if l[0] == "fold_change:":
			f_change = float(l[1])
		if l[0] == "p_value:":
			p_value = float(l[1])
	config.close()
	return file_list, p_value, f_change

def tsv_parser(tsv_file):
	tsv=open(tsv_file, "r")
	x = tsv.readline()
	x = x.translate({ord('\n'): None})
	y = x.split("\t")
	gene = y.index("\"baseMean\"") - 1
	fold = y.index("\"log2FoldChange\"")
	padj = y.index("\"padj\"")
	z = []
	for line in tsv:
		l = line.split("\t")
		m = (l[gene][1:-1], float(l[fold]), float(l[padj]))
		z.append(m)
	tsv.close()
	return z

def select_genes(select, genes, p, f):
	for i in genes:
		fo = abs(i[1])
		if i[2] < p and fo > f:
			select.append(i[0])
	return select

def set_genes(files, p_value, f_change):
	g = []
	for x in files:
		genes = tsv_parser(x)
		g = select_genes(g, genes, p_value, f_change)
	s = set(g)
	return s
	
def create_output(tsv_files, selection):
	f = open("results_filtered.csv", "w")
	header = ["gene"]
	alle = {}
	for x in tsv_files:
		name = x.split("/")[0]
		header.append(name+" f_change")
		header.append(name+" p_value")
		xx = tsv_parser(x)
		alle[x]=xx
	for q in header:
		f.write(q+",")
	f.write("\n")
	for y in selection:
		row=[]
		count = 1
		genename = y.split("|")[-1]
		row.append(genename)
		for key in alle:
			for a in alle[key]:
				if a[0] == y:
					row.append(round(a[1],2))
					row.append(round(a[2],3))
			count = count + 2
			if len(row) != count:
				row.append("none")
				row.append("none")
		print(row)		
		for r in row:
			f.write(str(r)+",")
		f.write("\n")
	f.close()
	
    
if __name__ == "__main__":

    # Step 1. Parse config file
    files, p_value, f_change = config_parser(argv[1])
    
    # Step 2.
    s = set_genes(files, p_value, f_change)
    
    #Step 3.
    create_output(files, s)
