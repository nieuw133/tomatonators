import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

def PCA_parser(in_fn):
    df = pd.read_csv(in_fn)
    whole_l=list(df)[1:]
    features=list(df['gene_id'])
    return df,whole_l,features

def metabolites_normalize(df,whole_l,features):
    sumdf = df.sum()[1:]
    sumdic = sumdf.to_dict()
    df_normalize = pd.DataFrame(columns=whole_l)
    print(df_normalize)
    for i in whole_l:
        df_normalize[i] = df[i].map(lambda x: math.log10(x) if x!=0 else x)
    df_normalize.index=features
    return df_normalize

def colum_select(df,whole_l,keywords):
    l=[]
    for j in keywords:
        for i in whole_l:
            if j in i:
                l+=[i]
    x = df.loc[:,l]
    return x,l

def PCA_process(x, l, features):
    x = StandardScaler().fit_transform(x.T)
    # print(pd.DataFrame(data = x, columns = features).head())
    pca = PCA(n_components=2)
    principalComponents = pca.fit_transform(x)
    pc=pca.fit(x).explained_variance_ratio_
    pc1,pc2=round(pc[0]*100,2),round(pc[1]*100,2)
    principalDf = pd.DataFrame(data = principalComponents, columns = ['principal component 1', 'principal component 2'])
    # print(principalDf.head(5))
    q=pd.DataFrame({'target':l})
    finalDf = pd.concat([principalDf, q], axis = 1)
    return finalDf,pc1,pc2

keywords=['cf','mock2','mz','mock1']
df,whole_l,features =PCA_parser('sample_counts.csv')
df_norm=metabolites_normalize(df,whole_l,features)
#
x,l=colum_select(df_norm,whole_l,keywords)
finalDf,pc1,pc2=PCA_process(x, l, features)
PCAdata_file_name=''
for i in keywords:
    PCAdata_file_name+=i.strip('_')
finalDf.to_csv(r'{}_PCA.csv'.format(PCAdata_file_name))
x.to_csv(r'{}_unchanged.csv'.format(PCAdata_file_name))


def PCA_plot(in_fn,pc1,pc2):
    finalDf = pd.read_csv(in_fn)
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_xlabel('Principal Component 1: {}%'.format(str(pc1)), fontsize=15)
    ax.set_ylabel('Principal Component 2: {}%'.format(str(pc2)), fontsize=15)
    ax.set_title('Sample Clustering', fontsize=20)

    targets = l
    color_dic = {}
    c = ['lawngreen','turquoise','red', 'orange']
    cn = 0
    q = 'cf'
    for i in l:
        if q in i:
            color_dic[i] = c[cn]
        else:
            cn += 1
            color_dic[i] = c[cn]
        q = i.split('_')[0]
    shape_plot_dic = {'12': 'o', '48': 's', '24': '*'}
    shape_dic = {}
    for i in l:
        for t in shape_plot_dic.keys():
            if t in i:
                shape_dic[i] = shape_plot_dic[t]
    print(targets)
    for target in targets:
        indicesToKeep = finalDf['target'] == target
        ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
                   , finalDf.loc[indicesToKeep, 'principal component 2']
                   , c=color_dic[target]
                   , s=60
                   , marker=shape_dic[target], edgecolor='black')
    ax.legend(targets, fontsize='x-small', title='Legend',loc='best')
    ax.grid()
    fig.savefig("{}.png".format(in_fn[:-4]))

PCA_plot('cfmock2mzmock1_PCA.csv',pc1,pc2)

