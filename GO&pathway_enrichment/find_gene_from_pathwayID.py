import pandas as pd

def pathway_finder(pathwayID):
    '''
    based on pathwayID retrieve all the genes involved from the eggnog annotation file and get their expression info: fold_change & p_value
    required file: KEGG_pathway.txt
                   query_seqs.fa.emapper.annotations (from eggnog prediciton)
                   differential expression files
    input: pathwayID, string, for example: 'map00909'
    output: tab-delimited text file contains all time points and all available fold_change & p_value
    '''

    #parse KEGG_pathway.txt, return dictionary
    dic = {}
    file = open('KEGG_pathway.txt', 'r')
    for line in file.readlines():
        line = line.strip()
        k = line.split('\t')[0][5:]
        v = line.split('\t')[1]
        dic[k] = v
    file.close()

    #parsing eggnog annotation file, retrieve dictionary {gene:['pathway annotation']}
    def eggnog_split(fn):
        f2 = pd.read_table(fn, header=None)
        f2 = f2.fillna(0)
        a = list(f2[0])
        b = list(f2[9])
        c = []
        d = []
        tardic = {}
        for i in range(len(a)):
            if a[i][5:7] != 00 and a[i] != 0 and b[i] != 0:
                c += [a[i]]
                d += [b[i]]
        for i in range(len(c)):
            tmp = []
            for j in d[i].split(','):
                if j.startswith("map"):
                    tmp += [j]
            tardic[c[i][:-2]] = tmp
        return tardic

    #reverse the dictionary aquired from eggnog_split to {pathwayID:['gene']}
    def GO_reverse(spices_dic):
        reversed_dic = {}
        for i in dic.keys():
            reversed_dic[i] = []
        for i, j in spices_dic.items():
            for p in j:
                reversed_dic[p] += [i]
        return reversed_dic


    tomato_genome_dic=eggnog_split('query_seqs.fa.emapper.annotations')
    tomato_genome_dic_reverse=GO_reverse(tomato_genome_dic)

    #get specific gene list involved in the pathway
    l=tomato_genome_dic_reverse[pathwayID]

    #get fold_change & p_value
    f1=pd.read_table('cf_mk2_12expressions.tsv')
    f2=pd.read_table("cf_mk2_24expressions.tsv")
    f3=pd.read_table("cf_mk2_48expressions.tsv")
    f4=pd.read_table("mz_mk1_12expressions.tsv")
    f5=pd.read_table("mz_mk1_24expressions.tsv")
    f6=pd.read_table("mz_mk1_48expressions.tsv")
    l_df=[f1,f2,f3,f4,f5,f6]
    dic_fold_change={}
    for i in l:
        dic_fold_change[i]=[]
    for i in l_df:
        for j in l:
            n = 0
            flag=False
            for values in list(i["gene"]):
                n+=1
                if j in values:
                    dic_fold_change[j]+=[i.loc[n]["log2FoldChange"]]
                    flag=True
            if flag==False:
                dic_fold_change[j] += [None]
    dic_padj={}
    for i in l:
        dic_padj[i]=[]
    for i in l_df:
        for j in l:
            n = 0
            flag=False
            for values in list(i["gene"]):
                n+=1
                if j in values:
                    dic_padj[j]+=[i.loc[n]["padj"]]
                    flag=True
            if flag==False:
                dic_padj[j] += [None]

    #write it into file
    f_out=open("{}_overview.txt".format(pathwayID),'w')
    f_out.write("gene\tcf_12\tcf_12p\tcf_24\tcf_24p\tcf_48\tcf_48p\tmz_12\tmz_12p\tmz_24p\tmz_24p\tmz_48\tmz_48p\n")
    for i,j in dic_fold_change.items():
        f_out.write(i+'\t'+str(j[0])+'\t'+str(dic_padj[i][0])+'\t'+str(j[1])+'\t'+str(dic_padj[i][1])+'\t'+str(j[2])+'\t'+str(dic_padj[i][2])+'\t'+str(j[3])+'\t'+str(dic_padj[i][3])+'\t'+str(j[4])+'\t'+str(dic_padj[i][4])+'\t'+str(j[5])+'\t'+str(dic_padj[i][5])+'\n')
    f_out.close()

pathway_finder('map00909')