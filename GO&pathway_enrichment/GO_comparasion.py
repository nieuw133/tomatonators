import pandas as pd
#need to change input file name everytime, apology for no workflow in this file ;)

#based on eggnog_GO_enrich and eggnog_KEGGpathway_enrich results perform inersection and union
#output -- file contain pathway or GO names, one name per line

go1=pd.read_csv('CF_MOCK2_12_kegg_results.csv',index_col=0)
go2=pd.read_csv('CF_MOCK2_24_kegg_results.csv',index_col=0)
go3=pd.read_csv('CF_MOCK2_48_kegg_results.csv',index_col=0)
print(go1,go2)
# go3 = go3.loc[go3['padj']<=0.05]
print(go1,go2)
# go1=go1.sort_values('padj',inplace=False)
# go2=go2.sort_values('padj',inplace=False)
# go3=go3.sort_values('padj',inplace=False)
l1=go1['GO_name'].tolist()
l2=go2['GO_name'].tolist()
l3=go3['GO_name'].tolist()
go1_go2_intersection=list(set(l1)&set(l2))
go123_inter=list(set(l3)&set(go1_go2_intersection))
go1_go2_all=list(set(l1).union(set(l2)))
go123_all=list(set(l3).union(set(go1_go2_all)))
go1_unique=list(set(l1).difference(set(go123_inter)))
go2_unique=list(set(l2).difference(set(go123_inter)))
go3_unique=list(set(l3).difference(set(go123_inter)))
print(len(l1),len(l2),len(go1_go2_all),len(go1_go2_intersection))

dic={'cf_12_unique':go1_unique,"cf_24_unique":go2_unique,'cf_48_unique':go3_unique,'cf_all':go123_all,'cf_intersection':go123_inter}
print(dic)
for i,j in dic.items():
    f=open('timepoint_kegg_'+i+".txt",'w')
    f.write('\n'.join(j))
    f.close
