#enviroment file:go-basic.obo

#parse GO file
f1=open('go-basic.obo','r')
def go_parse(f):
    curr=[]
    res=[]
    for line in f:
        if line.startswith('[Term]'):
            res+=[curr]
            curr=[]
        curr+=[line]
    if curr:
        res+=[curr]
    return res

l=go_parse(f1)
dic={}
for i in l:
    for j in i:
        if j.startswith("id: GO"):
            k=j.strip().strip("id: ")
        elif j.startswith("name: "):
            v=j.strip()[6:]
            dic[k] = v
        elif j.startswith("alt_id: "):
            k = j.strip()[8:]
            dic[k] = v

dic_isa={}
for i in dic.keys():
    dic_isa[i]=[]
for i in l:
    for j in i:
        if j.startswith("is_a: ") and j.strip().strip("is_a: ").split(" ")[0].startswith('GO:'):
            dic_isa[j.strip().strip("is_a: ").split(" ")[0]]+=[i[1].strip().strip("id: ")]

# save GO_dic GO_isa_dic
file = open('GO_dict.txt', 'w')
for k, v in dic.items():
    file.write(str(k) + '\t' + str(v) + '\n')
file.close()
file = open('GO_isa_dict.txt', 'w')
for k, v in dic_isa.items():
    file.write(str(k) + '\t' + ','.join(v) + '\n')
file.close()
