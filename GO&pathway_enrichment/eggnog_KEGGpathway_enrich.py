import pandas as pd
from scipy import stats
from statsmodels.stats import multitest

#enviroment file: KEGG_pathway.txt query_seqs.fa.emapper.annotations
#in_fn -- string, input file name, the file contains gene name, one gene name per line
#output -- csv file contains keggmap_ID/keggmap_name/p value/padj value/total gene in the keggmap/enriched gene in the keggmap

def pathway_enrichment(in_fn):
    dic = {}
    file = open('KEGG_pathway.txt','r')
    for line in file.readlines():
        line = line.strip()
        k = line.split('\t')[0][5:]
        v = line.split('\t')[1]
        dic[k] = v
    file.close()


    def eggnog_split(fn):
        f2=pd.read_table(fn,header=None)
        f2 = f2.fillna(0)
        a=list(f2[0])
        b=list(f2[9])
        # print(b)
        c=[]
        d=[]
        tardic={}
        for i in range(len(a)):
            if a[i][5:7]!=00 and a[i]!=0 and b[i]!=0:
                c+=[a[i]]
                d+=[b[i]]
        for i in range(len(c)):
            tmp=[]
            for j in d[i].split(','):
                if j.startswith("map"):
                    tmp+=[j]
            tardic[c[i][:-2]]=tmp
        return tardic


    def GO_reverse(spices_dic):
        reversed_dic={}
        for i in dic.keys():
            reversed_dic[i] = []
        for i, j in spices_dic.items():
            for p in j:
                reversed_dic[p]+=[i]
        return reversed_dic

    def enrichment_calc(target_dic,template_dic,n,k):
        enrich_prep={}
        enrich_result={}
        for i in target_dic.keys():
            m=len(target_dic[i])
            a=len(template_dic[i])
            if m == 0:
                enrich_prep[i] = [[0,0], [0,0]]
            else:
                enrich_prep[i] =[[m,a-m],[k-m,n-a-k+m]]
        for i,j in enrich_prep.items():
                enrich_result[i] = stats.fisher_exact(j)[1]
        l=[]
        adj={}
        l=multitest.multipletests(list(enrich_result.values()),alpha=0.05, method='fdr_bh')[1]
        for i in range(len(enrich_prep.keys())):
            adj[list(enrich_prep.keys())[i]]=l[i]
        return enrich_result,adj


    DE_gene_name=[]
    file = open(in_fn,'r')
    for line in file.readlines():
        line = line.strip()
        DE_gene_name += [line]
    file.close()

    tomato_genome_dic=eggnog_split('query_seqs.fa.emapper.annotations')
    l=[]
    for i in tomato_genome_dic.values():
        l+= i
    kegg_path=set(l)
    DE_dic={}
    for i in DE_gene_name:
        if i in tomato_genome_dic.keys():
            DE_dic[i]=tomato_genome_dic[i]

    n=len(tomato_genome_dic.keys())
    k=len(DE_dic.keys())
    target_dic=GO_reverse(DE_dic)
    template_dic=GO_reverse(tomato_genome_dic)
    results,adjresults=enrichment_calc(target_dic,template_dic,n,k)
    df_prep=[[],[],[],[],[],[]]
    for i,j in results.items():
        df_prep[0] += [i]
        df_prep[1] +=[dic[i]]
        df_prep[2] += [j]
        df_prep[3] += [adjresults[i]]
        df_prep[4] += [template_dic[i]]
        df_prep[5] += [target_dic[i]]
    df_dic = {'GO_ID':df_prep[0],'GO_name':df_prep[1],'p':df_prep[2], 'padj':df_prep[3]}
    df = pd.DataFrame.from_dict(df_dic)
    df=df.loc[df['padj']<0.01]
    df = df.sort_values('padj', inplace=False)
    print('enriched keggmap number:',df.shape[0])
    df.to_csv('{}_kegg_results_withoutgenename.csv'.format(in_fn[:-4]))
    return print('done')

pathway_enrichment('CF_MOCK2_48.txt')
pathway_enrichment('MZ_MOCK1_48.txt')
pathway_enrichment('CF_MOCK2_24.txt')
pathway_enrichment('MZ_MOCK1_24.txt')
pathway_enrichment('CF_MOCK2_12.txt')
pathway_enrichment('MZ_MOCK1_12.txt')
pathway_enrichment('CF_MOCK2_48_DR.txt')
pathway_enrichment('MZ_MOCK1_48_DR.txt')
pathway_enrichment('CF_MOCK2_24_DR.txt')
pathway_enrichment('MZ_MOCK1_24_DR.txt')
pathway_enrichment('CF_MOCK2_12_DR.txt')
pathway_enrichment('MZ_MOCK1_12_DR.txt')
pathway_enrichment('CF_MOCK2_48_UR.txt')
pathway_enrichment('MZ_MOCK1_48_UR.txt')
pathway_enrichment('CF_MOCK2_24_UR.txt')
pathway_enrichment('MZ_MOCK1_24_UR.txt')
pathway_enrichment('CF_MOCK2_12_UR.txt')
pathway_enrichment('MZ_MOCK1_12_UR.txt')
