import pandas as pd

#need to change input file name everytime, apology for no workflow in this file ;)

df=pd.read_table('mz_mk1_48expressions.tsv',index_col=0)

c=df['padj'].to_dict()
fclog=df['log2FoldChange'].to_dict()
slim_dic={}
for i,j in c.items():
    if j < 0.01:
        slim_dic[i.split('|')[1][0:16]]=fclog[i]

print(len(slim_dic))
file = open('MZ_MOCK1_48.txt', 'w')
for k,v in slim_dic.items():
    file.write(str(k)+'\n')
file.close()

