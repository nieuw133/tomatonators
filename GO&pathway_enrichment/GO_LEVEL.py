# create a file with GO terms level based on their network relationship is_a


dic = {}
dic_isa = {}
file = open('GO_dict.txt', 'r')
for line in file.readlines():
    line = line.strip()
    k = line.split('\t')[0]
    v = line.split('\t')[1]
    dic[k] = v
file.close()
file = open('GO_isa_dict.txt', 'r')
for line in file.readlines():
    g = line.strip()
    # print(g)
    k = g.split('\t')[0]
    if len(g.split('\t'))==2:
        v = g.split('\t')[1].split(',')
        dic_isa[k] = v
    else:
        dic_isa[k] = []
file.close()
# print(dic_isa)
dic_level={}
for i in dic.keys():
    dic_level[i]=0
for i in ['GO:0008150', 'GO:0000004', 'GO:0007582', 'GO:0044699', 'GO:0003674', 'GO:0005554', 'GO:0005575', 'GO:0008372']:
    dic_level[i]=1
    if dic_isa[i]!=[]:
        for j in dic_isa[i]:
            dic_level[j] = 2
            if dic_isa[j] != []:
                for k in dic_isa[j]:
                    dic_level[k] = 3
                    if dic_isa[k] != []:
                        for l in dic_isa[k]:
                            dic_level[l] = 4
                            if dic_isa[l] != []:
                                for m in dic_isa[l]:
                                    dic_level[m] = 5
                                    if dic_isa[m] != []:
                                        for n in dic_isa[m]:
                                            dic_level[n] = 6
                                            if dic_isa[n] != []:
                                                for o in dic_isa[n]:
                                                    dic_level[o] = 7
                                                    if dic_isa[o] != []:
                                                        for p in dic_isa[o]:
                                                            dic_level[p] = 8
                                                            if dic_isa[p] != []:
                                                                for q in dic_isa[p]:
                                                                    dic_level[q] = 9
                                                                    if dic_isa[q] != []:
                                                                        for r in dic_isa[q]:
                                                                            dic_level[r] = 10
                                                                            if dic_isa[r] != []:
                                                                                for s in dic_isa[r]:
                                                                                    dic_level[s] = 11
                                                                                    if dic_isa[s] != []:
                                                                                        for t in dic_isa[s]:
                                                                                            dic_level[t] = 12
                                                                                            if dic_isa[t] != []:
                                                                                                for u in dic_isa[t]:
                                                                                                    dic_level[u] = 13
                                                                                                    if dic_isa[u] != []:
                                                                                                        for v in dic_isa[u]:
                                                                                                            dic_level[v] = 14
                                                                                                            if dic_isa[
                                                                                                                v] != []:
                                                                                                                for w in \
                                                                                                                dic_isa[
                                                                                                                    v]:
                                                                                                                    dic_level[
                                                                                                                        w] = 15
                                                                                                                    if \
                                                                                                                    dic_isa[
                                                                                                                        w] != []:
                                                                                                                        for x in \
                                                                                                                                dic_isa[
                                                                                                                                    w]:
                                                                                                                            dic_level[
                                                                                                                                x] = 16
                                                                                                                            if \
                                                                                                                                    dic_isa[
                                                                                                                                        x] != []:
                                                                                                                                for y in \
                                                                                                                                        dic_isa[
                                                                                                                                            x]:
                                                                                                                                    dic_level[
                                                                                                                                        y] = 17
for i,j in dic_level.items():
    if j==0:
        print(i,j)
        for k,p in dic.items():
            if dic[i]==p and dic_level[k]!=0:
                dic_level[i]=dic_level[k]
file = open('GO_level_dict.txt', 'w')
for k, v in dic_level.items():
    file.write(str(k) + '\t' + str(v) + '\n')
file.close()